import React from 'react'
import * as BsCheck2 from "react-icons/bs";
import * as FcGoogle from "react-icons/fc";
import image from "../../assests/pic95.png"
import image1 from "../../assests/pic96.svg"
import image2 from "../../assests/pic97.png"
import image3 from "../../assests/pic98.png"
import image4 from "../../assests/pic99.png"

// import Col from 'react-bootstrap/Col';
import './LandingPage.css'

const LandingPage = () => {
    return (
        <div className='container-fluid'>

            <div className="row">
                <div className="col-md-8 styljira"><b>Roadmap Software</b></div>
                <div className="col-6 col-md-4 stylgit">get it free</div>
            </div>

            <div className='row-stylbac '>
                <div class=" stylcolordeiv">
                    <div className='textmain'>
                        <div>
                        <div className='software1'>The #1 software development tool used by agile teams</div>

                        <span className='each'>EACH PRODUCT ON A FREE PLAN:</span>
                        </div>
                        <div className='divtickdiv'>
                        <div className='styconsdiv'>
                            <BsCheck2.BsCheck2 />
                            <h6>Supports up to 10 users</h6>
                        </div>
                        <div className='styconsdiv'>
                            <BsCheck2.BsCheck2 />
                            <h6>Includes 2 GB storage</h6>
                        </div>
                        <div className='styconsdiv'>
                            <BsCheck2.BsCheck2 />
                            <h6>Offers Community support</h6>
                        </div>
                        <div className='styconsdiv'>
                            <BsCheck2.BsCheck2 />
                            <h6>Is always free, no credit card needed</h6>
                        </div>
                        </div>
                    </div>
                </div>
                <div class=" signgooglediv">
                    <div className='ContainerModl'>

                        <div style={{ color: 'black', fontSize: '40px' }}><b>Get started</b></div>
                        <div style={{ color: '#42526e', marginTop: '-0.5rem' }}>Free for up to 10 users</div>
                        <div className='btngoogdiv'>
                            <FcGoogle.FcGoogle />Continue with Google</div>
                        <div style={{ color: 'black', marginTop: '0.5rem' }}>OR</div>
                        <label className='landforSigndiv' for="myInput">Work Email</label>
                        <input id="myInput" type="text" className='fillthediv' />
                        <label className='landforSigndiv' for="myInput">Full Name</label>
                        <input id="myInput" type="text" className='fillthediv' />
                        <div className='Clickingbelow'>By clicking below, you agree to the Atlassian Cloud Terms of Service and Privacy Policy.</div>
                        <div className='btngoogdivs'>Agree</div>
                        <div className='Creditcrddiv'>NO CREDIT CARD REQUIRED</div>
                        <div className='rodmppdivdiv'>Roadmap</div>
                    </div>
                </div>
                <br />
            </div>
            <div className='writdiv'>
                <h2 className='jirrdiv'><b>Roadmap Your Way</b></h2>
                <h6 style={{ color: '#42526e' }} className='eslicustom'>Easily customize your workflows by turning on (or off) tons of
                    powerful features with a single click.</h6>
                <div className='imgdivall'>
                    <img src={image} className="jirimgdivall"></img>
                </div>
            </div>
            <div className='divfgriprojediv'>
                <div className='divwritediv'>We grew to 30 projects with lots of plans and branches under each, which means
                    50 running builds at the same time.
                    Considering the number of projects,
                    we couldn’t have survived without Atlassian.</div>
                <div className='divwritescott'>Scott Carpenter
                </div>
                <div className='divwritescotts' >Work Program Manager, Fugro Roames </div>
                <div className='imgfugdiv'>
                    <img src={image1} style={{ width: '12rem' }}></img>
                </div>
            </div>
            <div className='row-divunfunSet'>
                <div className=' unfunkdin'> <h4 style={{ color: 'black' }}>Unfunk your workflow</h4>
                    <div className='divSetupdiv' style={{ color: '#42526e' }}>Set up, clean up, and easily manage even the most hectic project workflows.</div></div>

                <div className=' divimgnextgen '>
                    <img src={image2} className="pic-image4" ></img></div>

            </div>
            <div className='row-imgRodmpdivStayTrack '>
                <div className='  '>
                    <img src={image3} className="imgrodmppdiv"></img>
                </div>
                <div className=' Staytrackondiv'>
                    <h4 style={{ color: 'black', width: '18rem' }}>Stay on track – even when the track changes</h4>
                    <div className='divSetupdivs' style={{ color: '#42526e' }}>Use new roadmaps to sketch out the big picture,
                        communicate plans with stakeholders,
                        and ensure your team stays on the same page.</div>
                </div>

            </div>
            <div className='row-divunfunSet'>
                <div className=' unfunkdin'> <h4 style={{ color: 'black' }}>Ditch the spreadsheets</h4>
                    <div className='divSetupdiv' style={{ color: '#42526e' }}>Keep every detail of a project centralized in real
                        time so up-to-date info can
                        flow freely across people, teams, and tools.</div></div>

                <div className=' divimgnextgen '>
                    <img src={image4} className="pic-images4" ></img></div>

            </div>

            <div className='ConcptTodiv'>
                <div> <h2 className='divlauntimediv'>Concept to launch in record time.</h2>
                    <div className='getfreediv'>Get it Free</div>
                </div>

            </div>
        </div>
    )
}

export default LandingPage